#include "ShaderConstants.fxh"

struct PS_Input
{
    float4 position : SV_Position;
    float4 color : COLOR;
    float2 texCoords : TEXCOORD_0;
#ifdef GEOMETRY_INSTANCEDSTEREO
	uint instanceID : SV_InstanceID;
#endif
#ifdef VERTEXSHADER_INSTANCEDSTEREO
	uint renTarget_id : SV_RenderTargetArrayIndex;
#endif
};

struct PS_Output
{
    float4 color : SV_Target;
};

ROOT_SIGNATURE
void main(in PS_Input PSInput, out PS_Output PSOutput)
{
    float fog = PSInput.color.a;

    float4 paletteLookupColor = TEXTURE_0.Sample(TextureSampler0, PSInput.texCoords);
    float2 paletteUV = float2(0, paletteLookupColor.r);
    float4 texColor =  TEXTURE_2.Sample(TextureSampler0, paletteUV);
    
    float4 defaultPaletteLookUpColor = TEXTURE_1.Sample(TextureSampler0, PSInput.texCoords);
    float2 defaultPaletteUV = float2(0, defaultPaletteLookUpColor.r);
    float4 defaultColor = TEXTURE_2.Sample(TextureSampler0, defaultPaletteUV);
    PSOutput.color.rgb = lerp( texColor, defaultColor, TIME ).rgb;
    PSOutput.color.a = fog;
}
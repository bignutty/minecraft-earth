#include "ShaderConstants.fxh"

struct PS_Input
{
    float4 position : SV_Position;
    float2 uv0 : TEXCOORD_0;
    float2 uv1 : TEXCOORD_1;
};

struct PS_Output
{
    float4 color : SV_Target;
};

void main( in PS_Input PSInput, out PS_Output PSOutput )
{
    float luma = TEXTURE_0.Sample(TextureSampler0, PSInput.uv0).r;
    float2 cbcr = TEXTURE_1.Sample(TextureSampler1, PSInput.uv1).rg;

    float4x4 ycbcrToRGBTransform = float4x4(
        float4(+1.0000, +1.0000, +1.0000, +0.0000),
        float4(+0.0000, -0.3441, +1.7720, +0.0000),
        float4(+1.4020, -0.7141, +0.0000, +0.0000),
        float4(-0.7010, +0.5291, -0.8860, +1.0000)
    );

    PSOutput.color = mul(ycbcrToRGBTransform, float4(luma, cbcr.x, cbcr.y, 1.0));
}
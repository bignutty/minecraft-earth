# minecraft-earth

An archive of all the Minecraft Earth game assets that got downloaded from the server.

## Directories and Files
- `genoa.mcpack` is the core resource pack used by the game. This should be the latest revision that got authored before the shutdown.
- `/genoa/` is an extracted version of the genoa.mcpack resource pack.
- `/assets/` contains all assets bundled with the latest release of Minecraft Earth on android (v0.33.0)
- `/res/` contains various other assets used by the android version of Minecraft Earth (v0.33.0)
- `/sounds/` contains the decoded contents of the games sound bank files, located under `/genoa/sounds/banks/`

This exists purely as an archive, meant for the sake of preservation. All copyright belongs to Mojang AB.